TARGET_DIR=/var/web/mlk
SERVER_DIR=root@thomaslabs.org:/var/www/html/mlk

CSS=$(wildcard *.css)
HTML=$(wildcard html/*/*.html) $(wildcard html/*.html)
BUILD=$(addprefix build/, $(HTML:html/%=%))

all: template.m4 $(BUILD)

build/% : html/%
	@mkdir -p $(dir $@)
	m4 -P template.m4 $< > $@

.PHONY: install
install:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_DIR)/media
	cp $(CSS) $(TARGET_DIR)
	rsync -r build/ $(TARGET_DIR)/
	rsync -r media/ $(TARGET_DIR)/media

.PHONY: upload
upload:
	scp -P 1764 $(CSS) $(SERVER_DIR)/
	rsync -Pre 'ssh -p 1764' build/ $(SERVER_DIR)/
	rsync -Pre 'ssh -p 1764' media/ $(SERVER_DIR)/media

.PHONY: clean
clean:
	rm $(BUILD)
