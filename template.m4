m4_define(`_WITH_TEMPLATE',
`<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/style.css"/>
<meta charset="utf-8">
</head>
<body>
<header>
<h1>Der Mittellandkanal</h1>
</header>
<nav>
<ul>
<li><a href="/">Home</a></li>
<li class="submenu">
<a href="/geschichte/">Geschichte</a>
<ul class="submenu-contents">
<li><a href="#">Subthema 1.1</a></li>
<li><a href="#">Subthema 1.2</a></li>
<li><a href="#">Subthema 1.3</a></li>
</ul>
</li>
<li class="submenu">
<a href="/ingenieurskunst/">Ingenieurskunst</a>
<ul class="submenu-contents">
<li><a href="/ingenieurskunst/bruecken.html">Brücken</a></li>
<li><a href="/ingenieurskunst/schleusen.html">Schleusen</a></li>
<li><a href="/ingenieurskunst/haefen.html">Häfen</a></li>
<li><a href="/ingenieurskunst/anderes.html">Anderes</a></li>
</ul>
</li>
<li class="submenu">
<a href="/leben_an_und_auf_dem_mlk/">Leben an und auf dem MLK</a>
<ul class="submenu-contents">
<li><a href="/leben_an_und_auf_dem_mlk/arbeiten_am_um_und_auf_dem_kanal.html">
Arbeiten am, um und auf dem Kanal
</a></li>
<li><a href="/leben_an_und_auf_dem_mlk/oekologie_und_natur_am_kanal.html">Ökologie und Natur am Kanal</a></li>
<li><a href="/leben_an_und_auf_dem_mlk/leben_am_kanal.html">Leben am Kanal</a></li>
</ul>
</li>
<li><a href="/naherholung_und_tourismus/">Naherholung und Tourismus</a></li>
<li><a href="/denkmalskultur/">Denkmalskultur</a></li>
</ul>
</nav>
<main>')m4_dnl
m4_define(`_END_TEMPLATE',
`</main>

<footer>
<a href="https://thomaslabs.org/privacy.html">Impressum</a>
<a href="https://thomaslabs.org/privacy.html">Datenschutz</a>
</footer>
</body>
</html>')m4_dnl
